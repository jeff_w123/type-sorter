## A NodeJs app to sort different types
<h3>Requirements:</h3>
- NodeJs Tested on Node 16

<h3>Install dependencies:</h3>
- npm install

<h3>Run app:</h3>
- node run

<h3>Run tests:</h3>
- mocha tests

<h3>Task Description: </h3> 
- Improve the utility code provided, designed to sort some related data types.
