const app = require('./app.js');


/*
* Data provided is already validated.
* Assume no error handling required.
* */
const testData = [
    {
        type: 'mailbox',
        value: 'bob@example.com',
    },
    {
        type: 'trusted-ip',
        value: '203.84.134.17',
    },
    {
        type: 'mail-server',
        value: '216.58.220.101',
    },
    {
        type: 'mail-server',
        value: '203.48.223.100',
    },
    {
        type: 'mailbox',
        value: 'big.bird@example.com',
    },
    {
        type: 'mailbox',
        value: 'jim@acme.com',
    },
    {
        type: 'trusted-ip',
        value: '222.88.133.177',
    }
];

console.log(app.sort(testData));
console.log(app.filterByType(testData, 'mailbox'));

// -- -- -- CONSOLE OUTPUT-- -- -- -- --

// node run
// [
//   { type: 'mail-server', value: '203.48.223.100' },
//   { type: 'mail-server', value: '216.58.220.101' },
//   { type: 'mailbox', value: 'jim@acme.com' },
//   { type: 'mailbox', value: 'big.bird@example.com' },
//   { type: 'mailbox', value: 'bob@example.com' },
//   { type: 'trusted-ip', value: '203.84.134.17' },
//   { type: 'trusted-ip', value: '222.88.133.177' }
// ]
// [
//   { type: 'mailbox', value: 'bob@example.com' },
//   { type: 'mailbox', value: 'big.bird@example.com' },
//   { type: 'mailbox', value: 'jim@acme.com' }
// ]
