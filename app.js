const app = {
    getDomain: (email) => {
        return email.substr(email.indexOf('@') + 1, email.length);
    },
    getLocalPart: (email) => {
        return email.substr(0, email.indexOf('@'));
    },
    compareStrings: (s1, s2) => {
        // js unicode comparison: 3 possible outputs
        // same => 0
        // asc => -1
        // desc => 1
        return (s1 > s2) - (s1 < s2);
    },
    stringComparator: (s1, s2) => {
        return app.compareStrings(s1.value, s2.value);
    },
    compareDomains: (email1, email2) => {
        return app.compareStrings(app.getDomain(email1), app.getDomain(email2));
    },
    compareLocalParts: (email1, email2) => {
        return app.compareStrings(app.getLocalPart(email1), app.getLocalPart(email2));
    },
    mailboxComparator: (email1, email2) => {
        return (
            // sort by domain then by localPart
            app.compareDomains(email1.value, email2.value)
            || app.compareLocalParts(email1.value, email2.value)
        )
    },
    getIpInt: (ip) => {
        // pad then parseInt e.g. 203.84.134.17 becomes 203084134017
        return parseInt(ip.split('.').map(part => {return part.padStart(3, '0');}).join(''));
    },
    ipComparator: (ip1, ip2) => {
        return app.getIpInt(ip1.value) - app.getIpInt(ip2.value);
    },
    getComparatorByType: (type) => {
        return [
                {
                    names: ['mailbox'],
                    compare: app.mailboxComparator
                },
                {
                    names: ['mail-server', 'trusted-ip'],
                    compare: app.ipComparator
                },
            ].find(c => c.names.indexOf(type) > -1).compare
    },

    /**
     * Returns collection with items not matching type removed.
     */
    filterByType: (collection, type) => {
        return collection.filter(item => item.type === type);
    },

    /**
     * Returns collection sorted by type,
     * then by type dependant comparator.
     */
    sort: (collection) => {

        const uniqueTypes = [...new Set(collection.map(item => item.type))];
        uniqueTypes.sort(app.compareStrings);

        return uniqueTypes.map(uniqueType => {
            return collection
                .filter(item => item.type === uniqueType)
                .sort(app.getComparatorByType(uniqueType));
        }).flat();
    }
};

module.exports = app;
