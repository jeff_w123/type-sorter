const assert = require('assert'),
    app = require('./app.js');


describe('Test app functions:', () => {

    it('should get the correct email domain for getDomain', (itCallback) => {

        // Arrange
        const testEmail = 'keysersoze@villain.com';
        const expectedDomain = 'villain.com';

        // Act
        const testResult = app.getDomain(testEmail);

        // Assert
        assert.equal(testResult, expectedDomain);
        itCallback();
    });

    it('should get the correct email localPart for getLocalPart', (itCallback) => {

        // Arrange
        const testEmail = 'keysersoze@villain.com';
        const expectedLocalPart = 'keysersoze';

        // Act
        const testResult = app.getLocalPart(testEmail);

        // Assert
        assert.equal(testResult, expectedLocalPart);
        itCallback();
    });

    it('should get the correct result for compareStrings', (itCallback) => {

        // Arrange
        const testString1 = 'alexisMachine';
        const testString2 = 'keyserSoze';
        const expectedIntRes = -1; // denotes ascending order

        // Act
        const testResult = app.compareStrings(testString1, testString2);

        // Assert
        assert.equal(testResult, expectedIntRes);
        itCallback();
    });

    it('should get the correct result for stringComparator with passed in objects', (itCallback) => {

        // Arrange
        const testObject1 = { value: 'alexisMachine' };
        const testObject2 = { value: 'keyserSoze' };
        const expectedIntRes = -1; // denotes ascending order

        // Act
        const testResult = app.stringComparator(testObject1, testObject2);

        // Assert
        assert.equal(testResult, expectedIntRes);
        itCallback();
    });

    it('should get the correct result for compareDomains with passed in emails', (itCallback) => {

        // Arrange
        const testEmail1 = 'gru@xxxdespicable.me';
        const testEmail2 = 'gru@despicable.me';
        const expectedIntRes = 1; // denotes descending order

        // Act
        const testResult = app.compareDomains(testEmail1, testEmail2);

        // Assert
        assert.equal(testResult, expectedIntRes);
        itCallback();
    });

    it('should get the correct result for compareLocalParts with passed in emails', (itCallback) => {

        // Arrange
        const testEmail1 = 'saru@starfleet.com';
        const testEmail2 = 'burnham@starfleet.com';
        const expectedIntRes = 1; // denotes descending order

        // Act
        const testResult = app.compareLocalParts(testEmail1, testEmail2);

        // Assert
        assert.equal(testResult, expectedIntRes);
        itCallback();
    });

    it('should compare domain first for mailboxComparator with passed in objects', (itCallback) => {

        // Arrange
        const testObj1 = { value: 'chakotay@voyager.com' };
        const testObj2 = { value: 'saru@discovery.com' };
        const expectedIntRes = 1; // denotes descending order

        // Act
        const testResult = app.mailboxComparator(testObj1, testObj2);

        // Assert
        assert.equal(testResult, expectedIntRes);
        itCallback();
    });

    it('should compare localParts for mailboxComparator when domains are the equal', (itCallback) => {

        // Arrange
        const testObj1 = { value: 'chakotay@starfleet.com' };
        const testObj2 = { value: 'saru@starfleet.com' };
        const expectedIntRes = -1; // denotes ascending order

        // Act
        const testResult = app.mailboxComparator(testObj1, testObj2);

        // Assert
        assert.equal(testResult, expectedIntRes);
        itCallback();
    });

    it('should get the correct result for getIpInt with passed in ip', (itCallback) => {

        // Arrange
        const testIp = '203.84.134.17';
        const expectedIntRes = 203084134017;

        // Act
        const testResult = app.getIpInt(testIp);

        // Assert
        assert.equal(testResult, expectedIntRes);
        itCallback();
    });

    it('should get the correct result for ipComparator with passed in objects', (itCallback) => {

        // Arrange
        const testObj1= { value: '203.184.134.117' };
        const testObj2 = { value: '203.84.134.17' };
        const expectedIntRes = 100000100;

        // Act
        const testResult = app.ipComparator(testObj1, testObj2);

        // Assert
        assert.equal(testResult, expectedIntRes);
        itCallback();
    });

    it('should get the correct result for getComparatorByType with passed in type', (itCallback) => {

        // Arrange
        const testType = 'mail-server';
        const expectedResult = app.ipComparator;

        // Act
        const testResult = app.getComparatorByType(testType);

        // Assert
        assert.equal(testResult, expectedResult);
        itCallback();
    });

    it('should get the correct result for filterByType with passed in collection and type', (itCallback) => {

        // Arrange
        const testType = 'mail-server';
        const testCollection = [
            { type: 'mail-server' },
            { type: 'mailbox' },
            { type: 'mail-server' }
        ];
        const expectedResult = [
            { type: 'mail-server' },
            { type: 'mail-server' }
        ];

        // Act
        const testResult = app.filterByType(testCollection, testType);

        // Assert
        assert.equal(JSON.stringify(testResult, null, 20), JSON.stringify(expectedResult, null, 20));
        itCallback();
    });

    it('should get the correct result for sort with passed in collection', (itCallback) => {

        // Arrange
        const testCollection = [
            { type: 'trusted-ip', value: '255.255.0.1'},
            { type: 'mail-server', value: '0.0.0.10'},
            { type: 'mailbox', value: 'jeff@plod.co.nz'},
            { type: 'mail-server', value: '0.0.0.9'},
            { type: 'trusted-ip', value: '254.255.0.1'},
            { type: 'mailbox', value: 'jeff@smx.co.nz'},
            { type: 'mailbox', value: 'support@plod.co.nz'},
        ];
        const expectedResult = [
            { type: 'mail-server', value: '0.0.0.9'},
            { type: 'mail-server', value: '0.0.0.10'},
            { type: 'mailbox', value: 'jeff@plod.co.nz'},
            { type: 'mailbox', value: 'support@plod.co.nz'},
            { type: 'mailbox', value: 'jeff@smx.co.nz'},
            { type: 'trusted-ip', value: '254.255.0.1'},
            { type: 'trusted-ip', value: '255.255.0.1'},
        ];

        // Act
        const testResult = app.sort(testCollection);

        // Assert
        assert.equal(JSON.stringify(testResult, null, 20), JSON.stringify(expectedResult, null, 20));
        itCallback();
    });
});
